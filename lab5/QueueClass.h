#pragma once

#include <iostream>

using namespace std;

class QueueClass {
    static const int SIZE = 10;
    char* queueClass;
    int front;
    int back;
public:
    QueueClass();

    void push(char elem);
    int size();
    void pop();
    char frontEl();
    char backEl();
};

QueueClass::QueueClass() {
    queueClass = new char[SIZE];
    front = back = 0;
}

void QueueClass::push(char elem) {
    if (back + 1 == front || (back + 1 == SIZE && !front)) {
        cout << "Full queue." << endl;
        return;
    }
    back++;
    if (back == SIZE) back = 0;
    queueClass[back] = elem;
}

void QueueClass::pop() {
    if (front == back) {
        cout << "Empty queue." << endl;
        return;
    }
    front++;
    if (front == SIZE) front = 0;
}

int QueueClass::size() {
    int s = 0;
    for (int i = front; i < back; i++)
        s++;
    return s;
}

char QueueClass::backEl() {
    return queueClass[back];
}

char QueueClass::frontEl() {
    return queueClass[front + 1];
}