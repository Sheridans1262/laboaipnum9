#include "TreeStruct.h"

struct TreeNode* AddTreeNode(int data, TreeNode* tree) {
    if (tree == NULL) {
        tree = new TreeNode(data);
    }
    else if (data == tree->data) {
        cout << "This element already exists." << endl;
        return(tree);
    }
    else  if (data < tree->data) {
        tree->left = AddTreeNode(data, tree->left);
    }
    else {
        tree->right = AddTreeNode(data, tree->right);
    }
    return(tree);
}

void OutputTree(TreeNode* TreeNode) {
    if (TreeNode != NULL) {
        OutputTree(TreeNode->left);
        cout << TreeNode->data << endl;
        OutputTree(TreeNode->right);
    }
}

int FindLeaf(int data, TreeNode* tree, int counter) {
    if (tree == NULL) {
        return -1;
    }
    else if (data == tree->data) {
        return counter;
    }
    else  if (data < tree->data) {
        counter++;
        counter = FindLeaf(data, tree->left, counter);
    }
    else {
        counter++;
        counter = FindLeaf(data, tree->right, counter);
    }
}