#include <iostream>
#include <string>
#include <fstream>
#include <queue>
#include "List.h"
//#include "Tree.h"
#include "TreeStruct.h"
#include "QueueClass.h"

using namespace std;

void taskTree();
void taskStack();
void taskList();
void taskListSorting();
void taskListElemDeleting();

int main()
{
	setlocale(LC_ALL, "rus");
	int menu_n;
	List<int> defIntList;

	cout << "Menu:" << endl;
	cout << "1. Create new list element" << endl <<
		"2. List output" << endl <<
		"3. TaskTree" << endl <<
		"4. TaskStack" << endl <<
		"5. TaskList" << endl <<
		"6. TaskListSorting" << endl <<
		"7. TaskListElemDeleting" << endl <<
		"8. Exit" << endl;
	while (true) {
		cout << "Choose an operation:" << endl;
		cin >> menu_n;
		switch (menu_n) {
		case 1: {
			int listElem;
			cout << "Enter new element: ";
			cin >> listElem;
			defIntList.push_back(listElem); 
		}
			break;
		case 2: {
			cout << "There are " << defIntList.GetSize() << " elements in the list." << endl;
			for (int i = 0; i < defIntList.GetSize(); i++) {
				cout << defIntList[i] << endl;
			}
			cout << endl;
		}
			  break;
		case 3: taskTree(); break;
		case 4: taskStack(); break;
		case 5: taskList(); break;
		case 6: taskListSorting(); break;
		case 7: taskListElemDeleting(); break;
		case 8: exit(1); break;
		default: cout << "Wrong number." << endl; break;
		}
	}
	
	return 0;
}

void taskTree() {
	TreeNode* defTree;
	defTree = NULL;

	int treeElemFinder, counter = 0;

	for (int i = 0; i < 15; i++) {
		defTree = AddTreeNode(rand() % 100, defTree);
	}
	OutputTree(defTree);

	cout << "Element you want to find: ";
	cin >> treeElemFinder;
	counter = FindLeaf(treeElemFinder, defTree, counter);
	if (counter == -1) { cout << "This element does not exists." << endl; }
	else { cout << "This element is on " << counter << " branch." << endl; }
}

void taskStack() {
	QueueClass defCharQueue;

	char buff[256];
	ifstream file1("TaskFile.txt");
	while (!file1.eof()) {
		file1 >> buff;
		cout << buff << endl;

		bool point = true;
		for (int i = 0; i < (strlen(buff) - 1) / 2; i++) {
			defCharQueue.push(buff[i]);
			defCharQueue.push(buff[strlen(buff) - 1 - i]);
			if (defCharQueue.frontEl() != defCharQueue.backEl()) {
				point = false;
			}
			defCharQueue.pop();
			defCharQueue.pop();
			if (!point) {
				cout << "This string is not symmetrical." << endl;
				break;
			}
		}
		if (point) {
			cout << "This string is symmetrical." << endl;
		}
	}
	file1.close();
}

void taskList() {
	List<string> defStrList;

	bool menu = true;
	string strElement;
	string vowels = "AEIOUaeiou";
	int listMenuNum;

	while (menu) {
		getline(cin, strElement);
		defStrList.push_back(strElement);
		cout << "Would you like to add another element? 1 - Yes, all other answers - No" << endl;
		cin >> listMenuNum;
		switch (listMenuNum) {
		case 1: break;
		default: menu = false; break;
		}
	}

	cout << "There are " << defStrList.GetSize() << " elements in the list." << endl;
	for (int i = 0; i < defStrList.GetSize(); i++) {
		cout << defStrList[i] << endl;
	}
	cout << endl;
	
	char* charUnit = nullptr;
	char charBuff[256] = "";
	int counter = 0;

	for (int i = 0; i < defStrList.GetSize(); i++) {
		charUnit = _strdup(defStrList[i].c_str());
		for (int j = 0; j < strlen(charUnit); j++) {
			for (int k = 0; k < 10; k++) {
				if (charUnit[j] == vowels[k]) {
					charBuff[counter] = vowels[k];
					counter++;
					break;
				}
			}
		}
		cout << charBuff << endl;
		counter = 0;
	}

	delete[] charUnit;
}

void taskListSorting() {
	List<string> defStrListTask6;

	int chooseYourSide;
	string buff;

	ifstream file1("TaskFile.txt");
	while (!file1.eof()) {
		getline(file1, buff);
		defStrListTask6.push_back(buff);
	}
	file1.close();

	cout << "Do you want to sort this strings? 1 - Yes";
	cin >> chooseYourSide;
	if (chooseYourSide == 1) {
		int listSize = defStrListTask6.GetSize();
		string* buffArray = new string[listSize];
		ifstream file2("TaskFile.txt");
		for (int count = 0; count < listSize; count++) {
			getline(file2, buffArray[count]);
		}
		file2.close();

		string tmp;
		char *charFirst = nullptr, *charSecond = nullptr;
		for (int i = 0; i < listSize - 1; i++)
		{
			for (int j = 0; j < listSize - i - 1; j++)
			{
				charFirst = _strdup(buffArray[j].c_str());
				charSecond = _strdup(buffArray[j + 1].c_str());
				if (strcmp(charFirst, charSecond) > 0)
				{
					tmp = buffArray[j];
					buffArray[j] = buffArray[j + 1];
					buffArray[j + 1] = tmp;
				}
			}
		}
		delete charFirst;
		delete charSecond;

		defStrListTask6.clear();
		for (int count = 0; count < listSize; count++) {
			defStrListTask6.push_back(buffArray[count]);
		}
		delete[] buffArray;

		for (int i = 0; i < defStrListTask6.GetSize(); i++) {
			cout << defStrListTask6[i] << endl;
		}
	}
}

void taskListElemDeleting() {
	List<int> defIntTask7List1;
	List<int> defIntTask7List2;
	List<int> defIntTask7ListFinal;

	for (int i = 0; i < 50; i++) {
		defIntTask7List1.push_back(rand() % 100);
		defIntTask7List2.push_back(rand() % 100);
	}

	for (int precount1 = 0; precount1 < 50; precount1++) {
		cout << defIntTask7List1[precount1] << " ";
	}
	cout << endl << endl;
	for (int precount2 = 0; precount2 < 50; precount2++) {
		cout << defIntTask7List2[precount2] << " ";
	}
	cout << endl << endl;

	for (int count1 = 0; count1 < 50; count1++) {
		for (int count2 = 0; count2 < 50; count2++) {
			if (defIntTask7List1[count1] == defIntTask7List2[count2]) {
				defIntTask7ListFinal.push_back(defIntTask7List1[count1]);
				break;
			}
		}
	}

	for (int j = 0; j < defIntTask7ListFinal.GetSize(); j++) {
		cout << defIntTask7ListFinal[j] << " ";
	}
	cout << endl << endl;
}