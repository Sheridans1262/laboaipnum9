#pragma once

#include <iostream>

using namespace std;

struct TreeNode* AddTreeNode(int data, TreeNode* tree);
void OutputTree(TreeNode* TreeNode);
int FindLeaf(int data, TreeNode* tree, int counter);


struct TreeNode {
    int data;
    struct TreeNode* left;
    struct TreeNode* right;

    TreeNode(int data, TreeNode* left = NULL, TreeNode* right = NULL) {
        this->data = data;
        this->left = left;
        this->right = right;
    }
};