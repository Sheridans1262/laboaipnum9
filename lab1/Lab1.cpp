#include <iostream>

struct Buyer {
	int buyersNum : 32;
	char lastName[100], firstName[100], dadsName[100];
	char address[100];
	char phoneNum[20];
	char cardNum[20];
	enum fields {
		NAMES,
		ADDRESS,
		PHONENUM,
		CARDNUM
	};
};

void vyvodMaket(Buyer* buyers, int l);
void vvod(Buyer* buyers, int buyersLen); //1
void vyvod(Buyer* buyers, int buyersLen); //2
void sorting(Buyer* buyers, int buyersLen); //3
void pathfinder(Buyer* buyers, int buyersLen); //4
void change(Buyer* buyers); //5
void deleteStruc(Buyer* buyers, int buyersLen); //6
void vyvodAll(Buyer* buyers, int buyersLen); //7

void main()
{
	setlocale(LC_ALL, "rus");
	int menu_n, buyersLen = 1;
	Buyer* buyers = new Buyer[buyersLen];

	std::cout << "Menu:" << std::endl;
	std::cout << "1. Create new structure" << std::endl <<
		"2. Show the structure" << std::endl <<
		"3. Sort an array of structures" << std::endl <<
		"4. Search in an array of structures" << std::endl <<
		"5. Change the structure" << std::endl <<
		"6. Delete the structure" << std::endl <<
		"7. Show an array of structures" << std::endl <<
		"8. Exit" << std::endl;
	while (true) {
		std::cout << "Choose an operation:" << std::endl;
		std::cin >> menu_n;
		switch (menu_n) {
		case 1: buyersLen++; vvod(buyers, buyersLen - 1); break;
		case 2: vyvod(buyers, buyersLen - 1); break;
		case 3: sorting(buyers, buyersLen - 1); break;
		case 4: pathfinder(buyers, buyersLen - 1); break;
		case 5: change(buyers);  break;
		case 6: deleteStruc(buyers, buyersLen - 1);  break;
		case 7: vyvodAll(buyers, buyersLen - 1); break;
		case 8: exit(1); break;
		default: std::cout << "Wrong number." << std::endl; break;
		}
	}
}

void vyvodMaket(Buyer* buyers, int l) {
	std::cout << "# " << buyers[l].buyersNum << std::endl;
	std::cout << "Last name, first name and dad's name: " << buyers[l].lastName << " " << buyers[l].firstName << " " << buyers[l].dadsName << " " << std::endl;
	std::cout << "Buyer's adress: " << buyers[l].address << std::endl;
	std::cout << "Buyer's phone number: " << buyers[l].phoneNum << std::endl;
	std::cout << "Buyer's card number: " << buyers[l].cardNum << std::endl << std::endl;
}

void vvod(Buyer* buyers, int buyersLen) { //1
	buyers[buyersLen - 1].buyersNum = buyersLen;
	std::cout << "Enter last name, first name and dad's name: ";
	std::cin >> buyers[buyersLen - 1].lastName >> buyers[buyersLen - 1].firstName >> buyers[buyersLen - 1].dadsName;
	std::cout << "Enter buyer's adress: ";
	std::cin >> buyers[buyersLen - 1].address;
	std::cout << "Enter buyer's phone number: ";
	std::cin >> buyers[buyersLen - 1].phoneNum;
	std::cout << "Enter buyer's card number: ";
	std::cin >> buyers[buyersLen - 1].cardNum;
	std::cout << std::endl;
}

void vyvod(Buyer* buyers, int buyersLen) { //2
	int v;
	std::cout << "Enter buyer's number: ";
	std::cin >> v;
	if (v <= buyersLen && v > 0) {
		vyvodMaket(buyers, v - 1);
	}
	else std::cout << "Err";
}

void sorting(Buyer* buyers, int buyersLen) { //3
	Buyer tmp;
	for (int i = 0; i < buyersLen - 1; i++)
	{
		for (int j = 0; j < buyersLen - i - 1; j++)
		{
			if (strcmp(buyers[j].lastName, buyers[j + 1].lastName) > 0)
			{
				tmp = buyers[j];
				buyers[j] = buyers[j + 1];
				buyers[j + 1] = tmp;
			}
		}
	}
}

void pathfinder(Buyer* buyers, int buyersLen) { //4
	int path, findCount = 0; char finder[100];
	std::cout << "Enter the field you need: ";
	std::cin >> path;
	std::cout << std::endl;
	std::cout << "Enter the phrase: ";
	std::cin >> finder;
	std::cout << std::endl;
	switch (path - 1) {
	case Buyer::NAMES: {
		for (int l = 0; l < buyersLen; l++) {
			if (strcmp(buyers[l].firstName, finder) == 0) {
				vyvodMaket(buyers, l);
				findCount++;
				break;
			}
		}
		if (findCount == 0) {
			for (int l = 0; l < buyersLen; l++) {
				if (strcmp(buyers[l].lastName, finder) == 0) {
					vyvodMaket(buyers, l);
					findCount++;
					break;
				}
			}
		}
		if (findCount == 0) {
			for (int l = 0; l < buyersLen; l++) {
				if (strcmp(buyers[l].dadsName, finder) == 0) {
					vyvodMaket(buyers, l);
					findCount++;
					break;
				}
			}
		}
		if (findCount == 0) std::cout << "This buyer does not exists." << std::endl;
	}
		  break;
	case Buyer::ADDRESS: {
		for (int l = 0; l < buyersLen; l++) {
			if (strcmp(buyers[l].address, finder) == 0) {
				vyvodMaket(buyers, l);
				findCount++;
				break;
			}
		}
		if (findCount == 0) std::cout << "This buyer does not exists." << std::endl;
	}
		  break;
	case Buyer::PHONENUM: {
		for (int l = 0; l < buyersLen; l++) {
			if (strcmp(buyers[l].phoneNum, finder) == 0) {
				vyvodMaket(buyers, l);
				findCount++;
				break;
			}
		}
		if (findCount == 0) std::cout << "This buyer does not exists." << std::endl;
	}
		  break;
	case Buyer::CARDNUM: {
		for (int l = 0; l < buyersLen; l++) {
			if (strcmp(buyers[l].cardNum, finder) == 0) {
				vyvodMaket(buyers, l);
				findCount++;
				break;
			}
		}
		if (findCount == 0) std::cout << "This buyer does not exists." << std::endl;
	}
		  break;
	default: std::cout << "NO." << std::endl; break;
	}
}

void change(Buyer* buyers) { //5
	int ch;
	std::cout << "Enter buyer's number: ";
	std::cin >> ch;
	std::cout << "Last name, first name and dad's name: ";
	std::cin >> buyers[ch - 1].lastName >> buyers[ch - 1].firstName >> buyers[ch - 1].dadsName;;
	std::cout << "Enter buyer's adress: ";
	std::cin >> buyers[ch - 1].address;
	std::cout << "Enter buyer's phone number: ";
	std::cin >> buyers[ch - 1].phoneNum;
	std::cout << "Enter buyer's card number: ";
	std::cin >> buyers[ch - 1].cardNum;
	std::cout << std::endl;
}

void deleteStruc(Buyer* buyers, int buyersLen) { //6
	int d;
	std::cout << "Enter buyer's number: ";
	std::cin >> d;
	for (int i = d - 1; i < buyersLen - 1; i++) buyers[i] = buyers[i + 1];
	buyersLen--;
}

//better function that not working(

/*void deleteStruc(Buyer* buyers, int buyersLen) { //6
	int number, copyLen = buyersLen - 2;
	std::cout << "Enter buyer's number: ";
	std::cin >> number;
	for (int i = number - 1; i < buyersLen - 1; i++) { buyers[i] = buyers[i + 1]; }
	Buyer* buyersCopy = new Buyer[buyersLen - 1];
	for (int i = 0; i < copyLen; i++) { buyersCopy[i] = buyers[i]; }
	delete[] buyers;
	Buyer* buyers = new Buyer[buyersLen - 1];
	for (int i = 0; i < copyLen; i++) { buyers[i] = buyersCopy[i]; }
	buyersLen--;
}*/

void vyvodAll(Buyer* buyers, int buyersLen) { //7
	for (int i = 0; i < buyersLen; i++) {
		vyvodMaket(buyers, i);
	}
}
