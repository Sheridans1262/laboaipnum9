#include <iostream>
#include <string>

using namespace std;

void chessboard(int M, int N) {
	int** Matrix = new int* [M];
	for (int i = 0; i < M; i++) {
		Matrix[i] = new int[N];
		for (int j = 0; j < N; j++) {
			if (i % 2 != 0){
				if (j % 2 != 0){
					Matrix[i][j] = 0;
				}
				else{
					Matrix[i][j] = 1;
				}
				cout << Matrix[i][j] << " ";
			}
			else{
				if (j % 2 != 0){
					Matrix[i][j] = 1;
				}
				else{
					Matrix[i][j] = 0;
				}
				cout << Matrix[i][j] << " ";
			}
		}
		cout << endl;
	}
}

int pow(int num, int exp)
{
	if (exp == 0) {
		return 1;
	}
	if (exp > 0) {
		int result = 1;
		for (int i = 0; i < exp; ++i) {
			result *= num;
		}
		return result;
	}
	return -1;
}

unsigned int char2int(char chr)
{
	if (chr >= '0' && chr <= '9')
		return chr - '0';
	else if (chr >= 'A' && chr <= 'F')
		return chr - 'A' + 10;
	else if (chr >= 'a' && chr <= 'f')
		return chr - 'a' + 10;
	return -1;
}

int  hexToDec(const char* hex, int size)
{
	int dec = 0;
	for (int j = 0, i = size - 1; j < size; ++j, --i) {
		dec += char2int(hex[j]) * pow(16, i);
	}
	return dec;
}

int fib2(int num) {
	if (num == 0) return 1;
	if (num == 1) return 1;
	return fib2(num - 2) + fib2(num - 1);
}

int main() {
	setlocale(LC_ALL, "rus");

	//1
	cout << "��������� �����. ������� ���-�� ����� � ��������:";
	int columns, strings;
	cin >> columns >> strings;
	chessboard(strings, columns);

	//2
	cout << "������� �� 16 � 10. ������� 16-������ �����:";
	string hex;
	cin >> hex;
	int dec = hexToDec(hex.c_str(), hex.length());
	cout << dec << endl;

	//3
	cout << "������������������ ���������." << endl;
	int fibNum;
	int fibona44i[20] = { 0 };
	fibona44i[0] = { 1 };
	fibona44i[1] = { 1 };
	for (int i = 0; i < 5; i++) {
		cout << "������� ����� �����:";
		cin >> fibNum;
		if (fibNum == 1) { cout << fibona44i[0]; continue; }
		if (fibNum == 2) { cout << fibona44i[1]; continue; }
		if (fibona44i[fibNum - 1] == 0) {
			fibona44i[fibNum - 1] = fib2(fibNum - 1);
			cout << fibona44i[fibNum - 1] << endl;
		}
		else {
			cout << fibona44i[fibNum - 1] << endl;
		}
	}
}