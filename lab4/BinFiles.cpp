#include <iostream>
#include <fstream>

struct Worker {
	int workUnitNum;
	char lastName[100], firstName[100], dadsName[100];
	char postIndex[6];
	char country[20];
	char city[20];
	char street[20];
	int homeNum;
	int flatNum;
	char nationality[20];
	char birthday[10];
	char education[40];
	int firstWorkingYear;
};

void fileCreating(char*);
void elementCreating(char*, int);
void fileOutput (char*, int);
int elementDeleting(char*, int);
void elementSorting(char*, int);

void main()
{
	setlocale(LC_ALL, "rus");
	int menu_n, workersCounter = 0;
	char FileName[256];

	std::cout << "Menu:" << std::endl;
	std::cout << "1. Create new file" << std::endl <<
		"2. Create new element" << std::endl <<
		"3. Show an information from file" << std::endl <<
		"4. Delete the element" << std::endl <<
		"5. Sort an array of structures" << std::endl <<
		"6. Exit" << std::endl;
	while (true) {
		std::cout << "Choose an operation:" << std::endl;
		std::cin >> menu_n;
		switch (menu_n) {
		case 1: {
			std::cout << "Enter the name of the file (with resolution): ";
			std::cin >> FileName;
			fileCreating(FileName);
		};
			  break;
		case 2: workersCounter++; elementCreating(FileName, workersCounter); break;
		case 3: fileOutput(FileName, workersCounter); break;
		case 4: workersCounter = elementDeleting(FileName, workersCounter); break;
		case 5: elementSorting(FileName, workersCounter); break;
		case 6: exit(1); break;
		default: std::cout << "Wrong number." << std::endl; break;
		}
	}
}

void fileCreating(char* FileName) {
	std::ofstream tmp(FileName);
	tmp.close();
}

void elementCreating(char* FileName, int UnitNum) {
	Worker pack;

	std::ofstream file;
	file.open(FileName, std::ofstream::app);
	if (!file.is_open()) {
		std::cout << "File isn't open!";
	}
	else {
		pack.workUnitNum = UnitNum;
		std::cout << "Enter worker's last name: ";
		std::cin >> pack.lastName;
		std::cout << "Enter worker's first name: ";
		std::cin >> pack.firstName;
		std::cout << "Enter worker's dads name: ";
		std::cin >> pack.dadsName;
		std::cout << "Enter worker's post index: ";
		std::cin >> pack.postIndex;
		std::cout << "Enter worker's homecountry: ";
		std::cin >> pack.country;
		std::cout << "Enter worker's city: ";
		std::cin >> pack.city;
		std::cout << "Enter worker's street: ";
		std::cin >> pack.street;
		std::cout << "Enter worker's home number: ";
		std::cin >> pack.homeNum;
		std::cout << "Enter worker's flat number: ";
		std::cin >> pack.flatNum;
		std::cout << "Enter worker's nationality: ";
		std::cin >> pack.nationality;
		std::cout << "Enter worker's birthday: ";
		std::cin >> pack.birthday;
		std::cout << "Enter worker's education: ";
		std::cin >> pack.education;
		std::cout << "Enter worker's first working year: ";
		std::cin >> pack.firstWorkingYear;

		file.write((char*)&pack, sizeof(Worker));
	}
	file.close();
}

void fileOutput(char* FileName, int valueOfWorkers) {
	Worker pack;

	std::ifstream file;
	file.open(FileName);
	if (!file.is_open()) {
		std::cout << "File isn't open!";
	}
	else {
		for (int i = 0; i < valueOfWorkers; i++) {
			file.read((char*)&pack, sizeof(Worker));
			std::cout << pack.workUnitNum << std::endl;
			std::cout << pack.lastName << " " << pack.firstName << " " << pack.dadsName
				<< " " << pack.postIndex << " " << pack.country << " " << pack.city
				<< " " << pack.street << " " << pack.homeNum << " " << pack.flatNum
				<< " " << pack.nationality << " " << pack.birthday << " " << pack.education
				<< " " << pack.firstWorkingYear;
			std::cout << std::endl << std::endl
				<< "####################################################"
				<< std::endl << std::endl;
		}
	}
	file.close();
}

int elementDeleting(char* FileName, int valueOfWorkers) {
	Worker* packArray = new Worker[valueOfWorkers];
	int elemDeleting;

	std::ifstream file1;
	file1.open(FileName);
	if (!file1.is_open()) {
		std::cout << "File isn't open!";
	}
	else {
		std::cout << "Enter the element's number to delete: ";
		while (true) {
			std::cin >> elemDeleting;
			if (elemDeleting > valueOfWorkers || elemDeleting <= 0) { std::cout << "This element doesn't exist. Try again:"; }
			else break;
		}

		for (int i = 0; i < valueOfWorkers; i++) {
			file1.read((char*)&packArray[i], sizeof(Worker));
		}
		file1.close();

		for (int i = elemDeleting - 1; i < valueOfWorkers - 1; i++) {
			packArray[i] = packArray[i + 1];
			packArray[i].workUnitNum--;
		}
		valueOfWorkers--;

		std::ofstream file2;
		file2.open(FileName);
		for (int i = 0; i < valueOfWorkers; i++) {
			file2.write((char*)&packArray[i], sizeof(Worker));
		}
		file2.close();
	}

	if (file1.is_open()) {
		file1.close();
	}
	
	delete[] packArray;

	return valueOfWorkers;
}

void elementSorting(char* FileName, int valueOfWorkers) {
	Worker* packArray = new Worker[valueOfWorkers];

	std::ifstream file1;
	file1.open(FileName);
	if (!file1.is_open()) {
		std::cout << "File isn't open!";
	}
	else {
		for (int i = 0; i < valueOfWorkers; i++) {
			file1.read((char*)&packArray[i], sizeof(Worker));
		}
		file1.close();

		Worker tmp;
		for (int i = 0; i < valueOfWorkers - 1; i++)
		{
			for (int j = 0; j < valueOfWorkers - i - 1; j++)
			{
				if (strcmp(packArray[j].lastName, packArray[j + 1].lastName) > 0)
				{
					tmp = packArray[j];
					packArray[j] = packArray[j + 1];
					packArray[j + 1] = tmp;
				}
			}
		}

		std::ofstream file2;
		file2.open(FileName);
		for (int i = 0; i < valueOfWorkers; i++) {
			file2.write((char*)&packArray[i], sizeof(Worker));
		}
		file2.close();
	}

	if (file1.is_open()) {
		file1.close();
	}

	delete[] packArray;
}