#include <iostream>
#include <fstream>

using namespace std;

void matrixCreating(int, int, char*);
void addition(int, int, char*);
void output(int, int, char*);

int main()
{
	int firstNumOfMatrix, secondNumOfMatrix, indexMatrix, difference;
	char file1[256] = "File1.txt", file2[256] = "File2.txt";

	cout << "Number of matrices in 1st file: ";
	cin >> firstNumOfMatrix;
	cout << "Number of matrices in 2nd file: ";
	cin >> secondNumOfMatrix;
	cout << "Number of strings/columns in each matrix: ";
	cin >> indexMatrix;

	cout << endl << "First file: " << endl;
	matrixCreating(firstNumOfMatrix, indexMatrix, file1);
	cout << endl << "Second file: " << endl;
	matrixCreating(secondNumOfMatrix, indexMatrix, file2);

	difference = firstNumOfMatrix - secondNumOfMatrix;
	if (difference == 0) { return 0; };
	if (difference > 0) {
		addition(difference, indexMatrix, file2);
		cout << endl << "First file: " << endl;
		output(firstNumOfMatrix, indexMatrix, file1);
		cout << endl << "Second file: " << endl;
		output(firstNumOfMatrix, indexMatrix, file2);
		return 0;
	};
	if (difference < 0) {
		addition(abs(difference), indexMatrix, file1);
		cout << endl << "First file: " << endl;
		output(secondNumOfMatrix, indexMatrix, file1);
		cout << endl << "Second file: " << endl;
		output(secondNumOfMatrix, indexMatrix, file2);
		return 0;
	};
}

void matrixCreating(int numOfMatrix, int index, char* FileName) {
	double tmpNum;

	ofstream file;
	file.open(FileName);
	
	if (!file.is_open()) {
		std::cout << "File isn't open!";
	}
	else {
		for (int all = 0; all < numOfMatrix; all++) {
			for (int i = 0; i < index; i++) {
				for (int i = 0; i < index; i++) {
					tmpNum = (rand() % 1000) * 0.1;
					file << tmpNum << "\t";
					cout << tmpNum << "\t";
				}
				file << endl;
				cout << endl;
			}
			cout << endl;
		}
	}

	file.close();
}

void addition(int numOfMatrix, int index, char* FileName) {
	double tmpNum = 1;

	ofstream file;
	file.open(FileName, ofstream::app);

	if (!file.is_open()) {
		std::cout << "File isn't open!";
	}
	else {
		for (int all = 0; all < numOfMatrix; all++) {
			for (int i = 0; i < index; i++) {
				for (int i = 0; i < index; i++) {
					file << tmpNum << "\t";
				}
				file << endl;
			}
		}
	}

	file.close();
}

void output(int numOfMatrix, int index, char* FileName) {
	double tmpNum;

	ifstream file;
	file.open(FileName);

	if (!file.is_open()) {
		std::cout << "File isn't open!";
	}
	else {
		for (int all = 0; all < numOfMatrix; all++) {
			for (int i = 0; i < index; i++) {
				for (int i = 0; i < index; i++) {
					file >> tmpNum;
					cout << tmpNum << "\t";
				}
				cout << endl;
			}
			cout << endl;
		}
	}

	file.close();
}