#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

void task2();
void task3();
void task4();
void task5();

int main()
{
	setlocale(LC_ALL, "rus");
	int menuNum;
	void (*functions[4])() = {task2, task3, task4, task5};
	cout << "Menu:" << endl
		<< "1. Hello" << endl
		<< "2. Task 2" << endl
		<< "3. Task 3" << endl
		<< "4. Task 4" << endl
		<< "5. Task 5" << endl
		<< "6. Exit" << endl;
	while (true) {
		cout << "Enter the number:" << endl;
		cin >> menuNum;
		if (menuNum == 1) {
			cout << "Hi!" << endl;
			continue;
		}
		if (menuNum <= 0 || menuNum >= 7) {
			cout << "Wrong." << endl;
			continue;
		}
		if (menuNum == 6) exit(1);
		(*functions[menuNum - 2])();
	}
}

void task2() {
	int numOfStrings;
	cout << "Enter the number of strings in matrix: ";
	cin >> numOfStrings;

	ofstream file1("Task2file.txt");
	for (int i = 0; i < numOfStrings; i++) {
		for (int j = 0; j < numOfStrings; j++) {
			file1 << rand() << " ";
		}
		file1 << endl;
	}
	file1.close();

	int sizeOfMatrix = pow(numOfStrings, 2);
	int* tmpMatrixArray = new int[sizeOfMatrix];
	ifstream file2("Task2file.txt");
	for (int i = 0; i < sizeOfMatrix; i++) {
		file2 >> tmpMatrixArray[i];
	}
	file2.close();

	int tmp;
	for (int i = 0; i < sizeOfMatrix - 1; i++) {
		for (int j = 0; j < sizeOfMatrix - 1 - i; j++) {
			if (tmpMatrixArray[j] > tmpMatrixArray[j + 1]) {
				tmp = tmpMatrixArray[j];
				tmpMatrixArray[j] = tmpMatrixArray[j + 1];
				tmpMatrixArray[j + 1] = tmp;
			}
		}
	}

	ofstream file3("Task2file.txt", ios::app);
	file3 << endl;
	int count = 0;
	for (int i = 0; i < sizeOfMatrix; i++) {
		file3 << tmpMatrixArray[i] << " ";
		count++;
		if (count == numOfStrings) { file3 << endl; count = 0; }
	}
	file3.close();

	delete[] tmpMatrixArray;

	cout << "Done!" << endl;
}

void task3() {
	int numSymbols, num = 0;
	string buff;

	cout << "Enter the number of symbols to delete: ";
	cin >> numSymbols;

	ifstream file1("Task3file.txt");
	while (!file1.eof()) {
		getline(file1, buff);
		num++;
	}
	file1.close();

	string* buffArray = new string[num];

	ifstream file2("Task3file.txt");
	for (int i = 0; i < num; i++) {
		getline(file2, buffArray[i]);
		buffArray[i].erase(0, numSymbols);
	}
	file2.close();

	ofstream file3("Task3file.txt", ios::trunc);
	for (int i = 0; i < num; i++) {
		file3 << buffArray[i] << endl;
	}
	file3.close();

	delete[] buffArray;

	cout << "Done!" << endl;
}

void task4() {
	int length;
	cout << "Enter the num of letters: ";
	cin >> length;

	ofstream file1("Task4file.txt");
	file1 << "glaz rook blitz finka buck doc pulse frost capitao kaid twitch";
	file1.close();

	ifstream file2("Task4file.txt");
	ofstream file3("Task4fileOutput.txt");
	char buffer[100];
	for (int i = 0; !file2.eof(); i++) {
		file2 >> buffer;
		if (strlen(buffer) == length) {
			file3 << buffer << " ";
		}
	}
	file2.close();
	file3.close();
	cout << "Done!" << endl;
}

void task5() {
	ifstream file("Task5file.txt");
	vector<pair<char, int>> result;
	char tmp = file.get();

	while (tmp != EOF){
		int res = -1;
		for (int i = 0; i < result.size(); i++) { if (result[i].first == tmp) res = i; }
		if (res == -1) { result.push_back({ tmp, 1 }); }
		else result[res].second++;
		tmp = file.get();
	}

	file.close();
	sort(result.begin(), result.end());
	ofstream file1("Task5fileResult.txt");
	for (pair<char, int> item : result) { file1 << item.first << " - " << item.second << endl; }
	file1.close();
	cout << "Done!" << endl;
}